
Before raising an issue, please review the guidelines to determine the best approach:

* https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/contributing_guidelines/README.md

Please use one of the provided issue templates and include as much information as possible:
- Bug - To report a problem that can be reproduced.
- Question - To ask a question or report a bug that cannot be reproduced.
- Feature - To propose a new feature for the NuNet platform.
- Discussion - To start a discussion on a topic or share comments and thoughts.                   
- Issue - For internal use by the NuNet team to create issues following our software process.
- Default - This template holds this explanation.

Thank you for helping to make NuNet a globally decentralized computing framework!

<!-- template sourced from https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/.gitlab/issue_templates/Default.md -->
