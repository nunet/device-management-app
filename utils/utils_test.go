// Copyright 2024, Nunet
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.

package utils

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nunet/device-management-service/types"
)

func TestRandomString(t *testing.T) {
	t.Parallel()

	cases := map[string]struct {
		numChars int
		expErr   string
	}{
		"generate with zero characters": {
			numChars: 0,
		},
		"generate with two characters": {
			numChars: 2,
		},
		"generate with 1024 characters": {
			numChars: 1024,
		},
	}

	for name, tt := range cases {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			chars, err := RandomString(tt.numChars)
			if tt.expErr != "" {
				assert.EqualError(t, err, tt.expErr)
			} else {
				assert.Len(t, chars, tt.numChars)
			}
		})
	}
}

func TestIsExecutorStrictlyContained(t *testing.T) {
	executors1 := []interface{}{types.ExecutorTypeDocker, types.ExecutorTypeFirecracker, types.ExecutorTypeWasm}
	executors2 := []interface{}{types.ExecutorTypeDocker, types.ExecutorTypeFirecracker}
	executors3 := []interface{}{types.ExecutorTypeDocker}

	// possitive assertions
	assert.True(t, IsStrictlyContained(executors1, executors2), fmt.Sprintf("Executors %s strictly contains executors %s", executors1, executors2))
	assert.True(t, IsStrictlyContained(executors2, executors3), fmt.Sprintf("Executors %s strictly contains executors %s", executors2, executors3))

	// negative assertions
	assert.False(t, IsStrictlyContained(executors2, executors1), fmt.Sprintf("Executors %s strictly contains executors %s", executors2, executors1))
}

func TestIntersectionStringSlices(t *testing.T) {
	executors1 := []interface{}{types.ExecutorTypeDocker, types.ExecutorTypeFirecracker, types.ExecutorTypeWasm}
	executors2 := []interface{}{types.ExecutorTypeDocker, types.ExecutorTypeFirecracker}
	executors3 := []interface{}{types.ExecutorTypeDocker}
	executors4 := []interface{}{types.ExecutorTypeWasm}
	executors5 := []interface{}{types.ExecutorTypeFirecracker, types.ExecutorTypeWasm}
	executors6 := []interface{}{types.ExecutorTypeDocker, types.ExecutorTypeWasm}
	executors7 := []interface{}{types.ExecutorTypeFirecracker}

	// positive assertions
	assert.Equal(t, IntersectionSlices(executors1, executors2), executors2)
	assert.Equal(t, IntersectionSlices(executors5, executors6), executors4)
	assert.Equal(t, IntersectionSlices(executors2, executors7), executors7)

	// negative assertions
	assert.NotEqual(t, IntersectionSlices(executors1, executors2), executors3)
}

func TestIsSameShallowType(t *testing.T) {
	v1 := make(map[string]int)
	v1["1"] = 1
	v1["2"] = 2
	v1["3"] = 3

	v4 := make(map[string]int)
	v4["1"] = 5
	v4["2"] = 6
	v4["3"] = 7

	v2 := "string"
	v5 := "another string"
	v3 := float32(6.629)
	v6 := float32(7.9790)

	v7 := make(map[string]interface{})
	v7["1"] = 1
	v7["2"] = 3
	v7["3"] = 3

	v8 := make(map[string]interface{})
	v8["1"] = 5
	v8["2"] = 6
	v8["3"] = 7

	assert.True(t, IsSameShallowType(v1, v4))
	assert.True(t, IsSameShallowType(v2, v5))
	assert.True(t, IsSameShallowType(v3, v6))
	assert.True(t, IsSameShallowType(v7, v8))

	assert.False(t, IsSameShallowType(v1, v2))
	assert.False(t, IsSameShallowType(v2, v3))
}

func TestIsExecutorType(t *testing.T) {
	executorType1 := types.ExecutorTypeDocker
	executorType2 := types.ExecutorTypeFirecracker
	executorType3 := types.ExecutorTypeWasm

	// positive assertions
	assert.True(t, IsExecutorType(executorType1))
	assert.True(t, IsExecutorType(executorType2))
	assert.True(t, IsExecutorType(executorType3))

	// negative assertions
	assert.False(t, IsExecutorType("string"))
	assert.False(t, IsExecutorType(1))
}

func TestIsJobTypes(t *testing.T) {
	var jobType1 types.JobTypes
	jobType1 = append(jobType1, types.Batch)
	jobType1 = append(jobType1, types.SingleRun)

	var jobType2 types.JobTypes
	jobType2 = append(jobType2, types.Batch)
	jobType2 = append(jobType2, types.LongRunning)

	var jobType3 types.JobTypes
	jobType3 = append(jobType3, types.Recurring)
	jobType3 = append(jobType3, types.SingleRun)

	// positive assertions
	assert.True(t, IsJobTypes(jobType1))
	assert.True(t, IsJobTypes(jobType2))
	assert.True(t, IsJobTypes(jobType3))

	// negative assertions
	assert.False(t, IsJobTypes("string"))
	assert.False(t, IsJobTypes(1))
}

func TestIsJobType(t *testing.T) {
	jobType1 := types.Batch
	jobType2 := types.SingleRun
	jobType3 := types.LongRunning

	// positive assertions
	assert.True(t, IsJobType(jobType1))
	assert.True(t, IsJobType(jobType2))
	assert.True(t, IsJobType(jobType3))

	// negative assertions
	assert.False(t, IsJobType("string"))
	assert.False(t, IsJobType(1))
}

func TestConvertTypedSliceToUntypedSlice(t *testing.T) {
	var jobType1 types.JobTypes
	jobType1 = append(jobType1, types.Batch)
	jobType1 = append(jobType1, types.SingleRun)

	var jobType2 types.JobTypes
	jobType2 = append(jobType2, types.Batch)
	jobType2 = append(jobType2, types.LongRunning)

	var jobType3 types.JobTypes
	jobType3 = append(jobType3, types.Recurring)
	jobType3 = append(jobType3, types.SingleRun)

	// positive assertions
	actualValue := ConvertTypedSliceToUntypedSlice(jobType1)
	expectedValue := []interface{}{types.Batch, types.SingleRun}
	assert.Equal(t, expectedValue, actualValue)

	actualValue = ConvertTypedSliceToUntypedSlice(jobType2)
	expectedValue = []interface{}{types.Batch, types.LongRunning}
	assert.Equal(t, expectedValue, actualValue)

	actualValue = ConvertTypedSliceToUntypedSlice(jobType3)
	expectedValue = []interface{}{types.Recurring, types.SingleRun}
	assert.Equal(t, expectedValue, actualValue)
}

func TestIsGPUVendor(t *testing.T) {
	gpuVendor1 := types.GPUVendorNvidia
	gpuVendor2 := types.GPUVendorAMDATI
	gpuVendor3 := types.GPUVendorIntel

	// positive assertions
	assert.True(t, IsGPUVendor(gpuVendor1))
	assert.True(t, IsGPUVendor(gpuVendor2))
	assert.True(t, IsGPUVendor(gpuVendor3))

	// negative assertions
	assert.False(t, IsGPUVendor("string"))
	assert.False(t, IsGPUVendor(1))
}
