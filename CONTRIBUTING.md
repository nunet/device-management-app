# Contributing

First off, thanks for taking the time to contribute! :thumbsup:

## Code of Conduct
This project and everyone participating in it is governed by this [Code of Conduct](https://gitlab.com/nunet/device-management-service/-/blob/main/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Project License
When contributing to this project, you must agree that you have the necessary rights to the content and that the content you contribute may be provided under this [Project License](https://gitlab.com/nunet/device-management-service/-/blob/main/LICENSE).


## Contribution Guidelines 

Refer to [Contributing Guidelines](https://gitlab.com/nunet/team-processes-and-guidelines/-/blob/main/contributing_guidelines/README.md) document for details on how can you contribute to the project.




